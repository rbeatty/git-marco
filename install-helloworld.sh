#!/bin/bash

VERSION=3

LOCATION=/tmp
DESTINATION=${LOCATION}/HelloWorld

cd ${LOCATION}
wget --quiet https://raw.githubusercontent.com/redhat-benelux/git-foundation-workshop-binaries/master/v${VERSION}

mv ${LOCATION}/v${VERSION} ${DESTINATION}

chmod 755 ${DESTINATION}
echo "Hello"

exit 0
